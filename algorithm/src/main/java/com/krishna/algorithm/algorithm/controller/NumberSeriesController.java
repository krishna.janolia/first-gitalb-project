package com.krishna.algorithm.algorithm.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.krishna.algorithm.algorithm.Constant;
import com.krishna.algorithm.algorithm.UrlContent;
import com.krishna.algorithm.algorithm.service.NumberSeriesService;

/**
 * 
 * @author Krishna Janoliya this @Controller contains api of various number series
 */
@Controller
@RequestMapping(UrlContent.NUMBER_SERIES)
public class NumberSeriesController {
	
	@Autowired
	private NumberSeriesService numberSeriesService;
	/**
	 * api to get nth catalin number 
	 * @param value
	 * @return
	 */
	@GetMapping
	@RequestMapping(UrlContent.CATALIN_NUMBER)
	public ResponseEntity<int[]> getCatlinNumber(@RequestParam(Constant.VALUE) int value) {
		return new ResponseEntity<>(numberSeriesService.getCatlinNumber(value), HttpStatus.OK);
	}
	
	@GetMapping
	@RequestMapping(UrlContent.NT_ROOT)
	public ResponseEntity<Double> getNthRoot(@RequestParam(Constant.VALUE) int value,@RequestParam(Constant.ROOT_NUM) int rootNum) {
		return new ResponseEntity<>(numberSeriesService.getNthRoot(value,rootNum), HttpStatus.OK);
	}
	
}
