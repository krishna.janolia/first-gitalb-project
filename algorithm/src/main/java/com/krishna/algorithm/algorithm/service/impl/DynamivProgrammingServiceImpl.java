package com.krishna.algorithm.algorithm.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.krishna.algorithm.algorithm.entity.ValidationException;
import com.krishna.algorithm.algorithm.service.DynamicProgrammingService;

/**
 * service to implement various searching and sorting
 * @author Krishna Janoliya
 *
 */
@Service
public class DynamivProgrammingServiceImpl implements DynamicProgrammingService{

	@Override
	public Map<String, Object> minimumNumberOfMultiplicatioInMatrixChainMultipication(
			Map<String, Object> matrixDimentions) {
		List<Integer> matrixDimentionsArrayList =  (List<Integer>) matrixDimentions.get("dimentions");
		if(matrixDimentionsArrayList.size()<3) {
			 throw new ValidationException("please insert valid dimention for matrix's ");
		}
		int [] matrixDimentionsArray = new int[matrixDimentionsArrayList.size()];
		int index=0;
		for (int dimension : matrixDimentionsArrayList) {
			matrixDimentionsArray[index] = dimension;
			index++;
		}
		int dimentionsLenghth = matrixDimentionsArray.length;
		int [][] minMultiplications = new int [dimentionsLenghth-1][dimentionsLenghth-1];
		Map<String, Object> [][] sourceNodes = new Map [dimentionsLenghth-1][dimentionsLenghth-1];
		for(int i=0;i<dimentionsLenghth-1;i++) {
			minMultiplications[i][i]=0;
		}
		int i=0;
		int j=1;
		int l=1;
		boolean loopFlag = true;
		while(loopFlag) {
			String firstMatrixSequence="";
			String secondMatrixSequence="";
			int minMultiply=Integer.MAX_VALUE;
			int totalMultiplication;
			
			for(int k=i;k<j;k++) {
				totalMultiplication = minMultiplications[i][k]+
						minMultiplications[k+1][j] + 
						matrixDimentionsArray[i]
								*matrixDimentionsArray[k+1]*matrixDimentionsArray[j+1];
				if(totalMultiplication<minMultiply) {
					minMultiply=totalMultiplication;
					firstMatrixSequence=i+" "+k;
					secondMatrixSequence = k+1+" "+j;
				}
			}
			minMultiplications[i][j]=minMultiply;
			Map<String, Object> souceNodeUsedInMinimumMatrixMultiplication=new HashMap<String, Object>();
			souceNodeUsedInMinimumMatrixMultiplication.put("firstSourceNode", firstMatrixSequence);
			souceNodeUsedInMinimumMatrixMultiplication.put("secondSourceNode", secondMatrixSequence);
			sourceNodes[i][j]=souceNodeUsedInMinimumMatrixMultiplication;
			if(i==0&&j==dimentionsLenghth-2) {
				loopFlag = false;
			}
			if(j<dimentionsLenghth-2) {
				i++;
				j++;
			}else {
				i=0;
				l++;
				j=l;
			}
			minMultiply=Integer.MAX_VALUE;
		}
		Map<String, Object> result =new HashMap<>();
		result.put("minimumMultiplication", minMultiplications[0][dimentionsLenghth-2]);
		result.put("minMultiplicationArray", minMultiplications);
		result.put("minMultiplicationSourceNode", sourceNodes);
		return result;
	}
	
}
