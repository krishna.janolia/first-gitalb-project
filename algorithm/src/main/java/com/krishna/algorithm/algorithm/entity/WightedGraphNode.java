package com.krishna.algorithm.algorithm.entity;

import java.util.ArrayList;
import java.util.List;

public class WightedGraphNode {
	private int locationId;
	private int fromLocationId;
	private List<WightedGraphNode> connectedNodes = new ArrayList<>();
	private List<Integer> weightOfNodes = new ArrayList<>();
	private int weight;
	public int getLocationId() {
		return locationId;
	}
	public void setLocationId(int locationId) {
		this.locationId = locationId;
	}
	public int getFromLocationId() {
		return fromLocationId;
	}
	public void setFromLocationId(int fromLocationId) {
		this.fromLocationId = fromLocationId;
	}
	public List<WightedGraphNode> getConnectedNodes() {
		return connectedNodes;
	}
	public void setConnectedNodes(List<WightedGraphNode> connectedNodes) {
		this.connectedNodes = connectedNodes;
	}
	public List<Integer> getWeightOfNodes() {
		return weightOfNodes;
	}
	public void setWeightOfNodes(List<Integer> weightOfNodes) {
		this.weightOfNodes = weightOfNodes;
	}
	public int getWeight() {
		return weight;
	}
	public void setWeight(int weight) {
		this.weight = weight;
	}
	
	
}
