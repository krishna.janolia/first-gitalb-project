package com.krishna.algorithm.algorithm.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.krishna.algorithm.algorithm.Constant;
import com.krishna.algorithm.algorithm.UrlContent;
import com.krishna.algorithm.algorithm.entity.AvlTree;
import com.krishna.algorithm.algorithm.entity.Entity;
import com.krishna.algorithm.algorithm.entity.MyMap;
import com.krishna.algorithm.algorithm.entity.Tree;
import com.krishna.algorithm.algorithm.service.DataStructure;

/**
 * 
 * @author Krishna Janoliya this @Controller contains api of various searching
 *         and sorting algorithm
 */
@Controller
@RequestMapping(UrlContent.DATA_STRUCTURE)
public class DataStructureController {

	@Autowired
	private DataStructure dataStructure;

	/**
	 * this api create a linked list and value to linked list,it add value to
	 * existing linked list
	 * 
	 * @param value
	 * @return ResponseEntity<Map<String, Object>>
	 *
	 */
	@PostMapping
	@RequestMapping(UrlContent.ADD + UrlContent.NODE + UrlContent.LINKEDLIST + UrlContent.value)
	public ResponseEntity<Map<String, Object>> addElemrntToAvlTree(@PathVariable(Constant.VALUE) int value) {
		return new ResponseEntity<>(dataStructure.addNodeToLinkedList(value), HttpStatus.OK);
	}

	/**
	 * get element of linked list
	 * 
	 * @return ResponseEntity<Entity> :- Entity is class used to create linked list
	 */
	@GetMapping
	@RequestMapping(UrlContent.GET + UrlContent.LINKEDLIST)
	public ResponseEntity<Entity> getLinkedList() {
		return new ResponseEntity<>(dataStructure.getLinkedList(), HttpStatus.OK);
	}

	/**
	 * this api add node to a binary tree
	 * 
	 * @param value
	 * @return ResponseEntity<Tree> :- Tree is class used to create linked list
	 */
	@GetMapping
	@RequestMapping(UrlContent.ADD + UrlContent.NODE + UrlContent.TREE + UrlContent.value)
	public ResponseEntity<Tree> addTreeNode(@PathVariable(Constant.VALUE) int value) {
		return new ResponseEntity<>(dataStructure.addTreeNode(value), HttpStatus.OK);
	}

	/**
	 * this api delete node to a binary tree
	 * 
	 * @param value
	 * @return ResponseEntity<Tree> :- Tree is class used to create linked list
	 */
	@GetMapping
	@RequestMapping(UrlContent.DELETE + UrlContent.NODE + UrlContent.TREE + UrlContent.value)
	public ResponseEntity<Tree> getAndDeleteTreeNode(@PathVariable(Constant.VALUE) int value) {
		return new ResponseEntity<>(dataStructure.getAndDeleteTreeNode(value), HttpStatus.OK);
	}

	/**
	 * this api delete node to a AVLTREE tree
	 * 
	 * @param value
	 * @return @return ResponseEntity<AvlTree> :- AvlTree is class used to create
	 *         linked list
	 */
	@PostMapping
	@RequestMapping(UrlContent.ADD + UrlContent.NODE + UrlContent.AVLTREE + UrlContent.value)
	public ResponseEntity<AvlTree> addNodeToAvlTree(@PathVariable(Constant.VALUE) int value) {
		return new ResponseEntity<AvlTree>(dataStructure.addNodeToAvlTree(value), HttpStatus.CREATED);
	}

	/**
	 * this api create a map using hashing
	 * 
	 * @param key
	 * @param value
	 * @return ResponseEntity<MyMap[]> :- MyMap is class used to create implement
	 *         map
	 */
	@PostMapping
	@RequestMapping(UrlContent.ADD + UrlContent.NODE + UrlContent.MAP)
	public ResponseEntity<MyMap[]> addNodeToMap(@RequestParam(Constant.KEY) String key,
			@RequestParam(Constant.VALUE) String value) {
		return new ResponseEntity<MyMap[]>(dataStructure.addNodeToMap(key, value), HttpStatus.CREATED);
	}

}
