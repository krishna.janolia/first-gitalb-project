package com.krishna.algorithm.algorithm.entity;

public class AvlTree {
	private int value;
	private AvlTree left;
	private AvlTree right;
	public int getValue() {
		return value;
	}
	public void setValue(int value) {
		this.value = value;
	}
	public AvlTree getLeft() {
		return left;
	}
	public void setLeft(AvlTree left) {
		this.left = left;
	}
	public AvlTree getRight() {
		return right;
	}
	public void setRight(AvlTree right) {
		this.right = right;
	}
	
}
