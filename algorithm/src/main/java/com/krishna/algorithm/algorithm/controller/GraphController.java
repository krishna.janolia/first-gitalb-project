package com.krishna.algorithm.algorithm.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.krishna.algorithm.algorithm.Constant;
import com.krishna.algorithm.algorithm.UrlContent;
import com.krishna.algorithm.algorithm.service.GraphService;

/**
 * 
 * @author Krishna Janoliya this @Controller contains api of various graph searching
 *         and sorting algorithm
 */
@Controller
@RequestMapping(UrlContent.GRAPH)
public class GraphController {

	@Autowired
	private GraphService graphService;

	/**
	 * this api create a Graph
	 * 
	 * @param source
	 * @param destinetionn
	 * @return ResponseEntity<Boolean> :- return true if detination node successfully added
	 */
	@PostMapping
	@RequestMapping(UrlContent.ADD + UrlContent.NODE + UrlContent.GRAPH)
	public ResponseEntity<Boolean> addNodeToGraph(@RequestParam(Constant.SOURCE) int source,
			@RequestParam(Constant.DESTINETION) int destinetionn) {
		return new ResponseEntity<Boolean>(graphService.addNodeToGraph(source, destinetionn), HttpStatus.CREATED);
	}
	
	/**
	 * this api search value n graph using depth first search 
	 * @param value
	 * @return true if value exist in graph
	 */
	@GetMapping
	@RequestMapping(UrlContent.DFS)
	public ResponseEntity<Boolean> DFS(@RequestParam(Constant.VALUE) int value) {
		return new ResponseEntity<Boolean>(graphService.DFS(value), HttpStatus.CREATED);
	}
	
	/**
	 * this api search value n graph using Breath first search
	 * @param value
	 * @return true if value exist in graph
	 */
	@GetMapping
	@RequestMapping(UrlContent.BFS)
	public ResponseEntity<Boolean> BFS(@RequestParam(Constant.VALUE) int value) {
		return new ResponseEntity<Boolean>(graphService.BFS(value), HttpStatus.CREATED);
	}
	
	/**
	 * this api create a Eeighted Graph
	 * 
	 * @param source
	 * @param destinetionn
	 * @return ResponseEntity<Boolean> :- return true if detination node
	 *         successfully added
	 */
	@PostMapping
	@RequestMapping(UrlContent.ADD + UrlContent.NODE + UrlContent.WEIGHTED + UrlContent.GRAPH)
	public ResponseEntity<Boolean> addNodeToWeightedGraph(@RequestParam(Constant.SOURCE) int source,
			@RequestParam(Constant.DESTINETION) int destinetion, @RequestParam(Constant.WEIGHT) int weight) {
		return new ResponseEntity<Boolean>(graphService.addNodeToWeightedGraph(source, destinetion, weight),
				HttpStatus.CREATED);
	}
	
	/**
	 * this api create a Eeighted Graph
	 * 
	 * @param source
	 * @param destinetionn
	 * @return ResponseEntity<Boolean> :- return true if detination node
	 *         successfully added
	 */
	@PostMapping
	@RequestMapping(UrlContent.DIJKSTRA)
	public ResponseEntity<Integer> dijkstra(@RequestParam(Constant.SOURCE) int source,
			@RequestParam(Constant.DESTINETION) int destinetion) {
		return new ResponseEntity<Integer>(graphService.dijkstra(source, destinetion),
				HttpStatus.CREATED);
	}

}
