package com.krishna.algorithm.algorithm.service.impl;

import org.springframework.stereotype.Service;

import com.krishna.algorithm.algorithm.service.StringMatchingAlgorithmService;

@Service
public class StringMatchingAlgorithmSeviceImpl implements StringMatchingAlgorithmService{

	@Override
	public boolean match(String firstString, String secondString) {
		boolean result =false;
		int hashCodeOfSecondStringToBeMatched = getHashCodeOfStringToBeMatched(secondString);
		if(firstString.length()>=secondString.length()) {
			int hashCodeToBematched = 0;
			for(int i=0;i<secondString.length();i++) {
				hashCodeToBematched+=firstString.charAt(i)<<4;
			}
			int first=0;
			int last =secondString.length()-1;
			if (hashCodeToBematched == hashCodeOfSecondStringToBeMatched && secondString.length() == (last - first + 1)
					&& matchStringHavingSameHashCode(secondString, firstString, first, last)) {
				result=true;
			}else {
				
				for(int i=last+1;i<firstString.length();i++) {
					
					hashCodeToBematched-=firstString.charAt(first)<<4;
					hashCodeToBematched+=firstString.charAt(i)<<4;
					if (hashCodeToBematched == hashCodeOfSecondStringToBeMatched && matchStringHavingSameHashCode(secondString, firstString, first+1, i)) {
						result=true;
					}
					first++;
				}
			}
			
		}
		return result;
	}

	private boolean matchStringHavingSameHashCode(String secondString, String firstString, int first, int last) {
		boolean isSame=true;
		int firstStringIndex=0;
		for(int index=first;index<=last;index++) {
			if(firstString.charAt(index)!=secondString.charAt(firstStringIndex)) {
				isSame=false;
			}
			firstStringIndex++;
		}
		return isSame;
	}

	private int getHashCodeOfStringToBeMatched(String secondString) {
		int hashCode=0;
		for(int i=0;i<secondString.length();i++) {
			hashCode+=secondString.charAt(i)<<4;
		}
		return hashCode;
	}

}
  