package com.krishna.algorithm.algorithm.entity;

public class AvlTreeNode {
	private String value;
	private AvlTreeNode left;
	private AvlTreeNode right;
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public AvlTreeNode getLeft() {
		return left;
	}
	public void setLeft(AvlTreeNode left) {
		this.left = left;
	}
	public AvlTreeNode getRight() {
		return right;
	}
	public void setRight(AvlTreeNode right) {
		this.right = right;
	}
}
