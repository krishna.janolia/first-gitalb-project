package com.krishna.algorithm.algorithm.entity;


public class Entity {
	private int value;
	private Entity next;
	public int getValue() {
		return value;
	}
	public void setValue(int value) {
		this.value = value;
	}
	public Entity getNext() {
		return next;
	}
	public void setNext(Entity next) {
		this.next = next;
	}
	

}
