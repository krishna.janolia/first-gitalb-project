package com.krishna.algorithm.algorithm.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.krishna.algorithm.algorithm.Constant;
import com.krishna.algorithm.algorithm.UrlContent;
import com.krishna.algorithm.algorithm.service.StringMatchingAlgorithmService;

/**
 * 
 * @author Krishna Janoliya this @Controller contains api of various searching
 *         and sorting algorithm
 */
@Controller
@RequestMapping(UrlContent.ALGORITM)
public class StringMatchingAlgorithm {
	
	@Autowired
	private StringMatchingAlgorithmService stringMatchingAlgorithmService;

	@GetMapping
	@RequestMapping(UrlContent.MATCH + UrlContent.STRING)
	public ResponseEntity<Boolean> addElemrntToAvlTree(@RequestParam(Constant.FIRST_STRING) String firstString,
			@RequestParam(Constant.SECOND_STRING) String secondString) {
		return new ResponseEntity<>(stringMatchingAlgorithmService.match(firstString,secondString), HttpStatus.OK);
	}
}
