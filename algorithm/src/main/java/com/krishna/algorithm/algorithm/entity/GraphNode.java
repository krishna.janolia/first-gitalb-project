package com.krishna.algorithm.algorithm.entity;

import java.util.ArrayList;
import java.util.List;

public class GraphNode {
	private int value;
	private List<GraphNode> graphs = new ArrayList<>();
	public int getValue() {
		return value;
	}
	public void setValue(int value) {
		this.value = value;
	}
	public List<GraphNode> getGraphs() {
		return graphs;
	}
	public void setGraphs(List<GraphNode> graphs) {
		this.graphs = graphs;
	}
	
}
