package com.krishna.algorithm.algorithm.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.krishna.algorithm.algorithm.UrlContent;
import com.krishna.algorithm.algorithm.service.DynamicProgrammingService;

/**
 * 
 * @author Krishna Janoliya this @Controller contains api of various Dynamic
 *         Programming algorithm
 */
@Controller
@RequestMapping(UrlContent.DYNAMIC_PROGRAMMING)
public class DynamicProgamming {

	@Autowired
	private DynamicProgrammingService dynamicProgrammingService;
	
	/**
	 * this api create a linked list and value to linked list,it add value to
	 * existing linked list
	 * 
	 * @param value
	 * @return ResponseEntity<Map<String, Object>>
	 *
	 */
	@PostMapping
	@RequestMapping(UrlContent.MINIMUM_NUMBERR_OF_MULTIPLICATION_IN +UrlContent.MATRIX_CHAIN_MULTIPLICATION)
	public ResponseEntity<Map<String, Object>> minimumNumberOfMultiplicatioInMatrixChainMultipication(@RequestBody Map<String, Object> matrixDimentions){
		return new ResponseEntity<>(dynamicProgrammingService.minimumNumberOfMultiplicatioInMatrixChainMultipication(matrixDimentions), HttpStatus.OK);
	}

	
}
