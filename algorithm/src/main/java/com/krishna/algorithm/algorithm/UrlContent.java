package com.krishna.algorithm.algorithm;

public class UrlContent {
	public static final String ALGORITM ="/algorith";
	public static final String ADD = "/add";
	public static final String NODE = "/node";
	public static final String AVLTREE = "/avltree";
	public static final String value = "/{value}";
	public static final String LINKEDLIST = "/linkedlist";
	public static final String GET = "/get";
	public static final String TREE = "/tree";
	public static final String DELETE = "/delete";
	public static final String MAP = "/map";
	public static final String STRING = "/string";
	public static final String MATCH = "/match";
	public static final String DATA_STRUCTURE = "/datastructure";
	public static final String GRAPH = "/graph";
	public static final String NUMBER_SERIES = "/number_series";
	public static final String CATALIN_NUMBER = "catalin_number";
	public static final String NT_ROOT = "nt/root";
	public static final String DFS = "/DFS";
	public static final String BFS = "/BFS";
	public static final String WEIGHTED = "/weight";
	public static final String DIJKSTRA = "/dijktra";
	public static final String MATRIX_CHAIN_MULTIPLICATION = "/matix/chain/multiplication";
	public static final String MINIMUM_NUMBERR_OF_MULTIPLICATION_IN = "/minumum/number/of/multipication/in";
	public static final String DYNAMIC_PROGRAMMING="/dynamic/programming";
}
