package com.krishna.algorithm.algorithm.service;

import org.springframework.stereotype.Component;

@Component
public interface StringMatchingAlgorithmService{

	boolean match(String firstString, String secondString);

}
