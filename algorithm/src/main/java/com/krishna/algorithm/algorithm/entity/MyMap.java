package com.krishna.algorithm.algorithm.entity;

public class MyMap {
	private String key;
	private String value;
	private MyMap next;
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public MyMap getNext() {
		return next;
	}
	public void setNext(MyMap next) {
		this.next = next;
	}
	
}
