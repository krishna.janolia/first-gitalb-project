package com.krishna.algorithm.algorithm.service;

import org.springframework.stereotype.Component;


@Component
public interface GraphService {

	Boolean addNodeToGraph(int source, int destinetionn);

	Boolean BFS(int value);

	Boolean DFS(int value);

	Boolean addNodeToWeightedGraph(int source, int destinetionn, int weight);

	Integer dijkstra(int source, int destinetion);
	
}
