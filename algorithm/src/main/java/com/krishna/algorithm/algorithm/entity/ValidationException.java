package com.krishna.algorithm.algorithm.entity;

public class ValidationException extends RuntimeException {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String cause;

	public ValidationException(String cause) {
		super();
		this.cause = cause;
	}

	@Override
	public String toString() {
		return "ValidationException [cause=" + cause + "]";
	}
	
}
