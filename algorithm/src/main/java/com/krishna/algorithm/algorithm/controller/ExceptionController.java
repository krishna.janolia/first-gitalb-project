package com.krishna.algorithm.algorithm.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.krishna.algorithm.algorithm.entity.ValidationException;

@ControllerAdvice
public class ExceptionController {
	
	@ExceptionHandler(value = ValidationException.class)
	public ResponseEntity<String> productNotFound(ValidationException validationExcepion){
		return new ResponseEntity<>(validationExcepion.toString(), HttpStatus.BAD_REQUEST);
	}

}
