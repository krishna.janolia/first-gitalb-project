package com.krishna.algorithm.algorithm.service;

import java.util.Map;

import org.springframework.stereotype.Component;


@Component
public interface DynamicProgrammingService {

	Map<String, Object> minimumNumberOfMultiplicatioInMatrixChainMultipication(Map<String, Object> matrixDimentions);
	
}
