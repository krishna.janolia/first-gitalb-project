package com.krishna.algorithm.algorithm.service.impl;

import org.springframework.stereotype.Service;

import com.krishna.algorithm.algorithm.service.NumberSeriesService;

@Service
public class NumberSeriesServiceImpl implements NumberSeriesService{

	@Override
	public int[] getCatlinNumber(int value) {
		value++;
		int catlinNumberArray[]=new int [value];
		if(value>2) {
			for(int i=0;i<2;i++) {
				catlinNumberArray[i]=1;
			}
			for(int i=2;i<value;i++) {
				int sum=0;
				for(int j=0;j<i;j++) {
					sum+=catlinNumberArray[j]*catlinNumberArray[i-j-1];
				}
				catlinNumberArray[i]=sum;
			}
		}else {
			for(int i=0;i<value;i++) {
				catlinNumberArray[i]=1;
			}
		}
		return catlinNumberArray;
	}

	@Override
	public double getNthRoot(int value, int rootNum) {
		int end=value;
		double result=value;
		if(rootNum<0) {
			result=-1;
		}else if(rootNum>1) {
			boolean perfectSequare=false;
			for(int i=1;i<value/rootNum;i=i*2) {
				end	=i;
				double power=getPower(i,rootNum);
				if(power==value) {
					result=i;
					perfectSequare=true;
					break;
				}else if(power>value) {
					break;
				}
			}
			if(!perfectSequare) {
				for(int i=(end/2-1);i<value/rootNum;i++) {
					end=i;
					double power=getPower(i,rootNum);
					if(power==value) {
						result=i;
						perfectSequare=true;
						break;
					}else if(power>value) {
						break;
					}
				}
				if(!perfectSequare) {
					double lowerLimit=end-1;
					double upperLimit=end;
					double diff=value;
					
					while(diff>0.00001) {
						double mid=(lowerLimit+upperLimit)/2;
						result=mid;
						double power = getPower(mid,rootNum);
						if(power==value) {
							break;
						}
						if(power>value) {
							upperLimit=mid;
							diff=power-value;
						}else {
							lowerLimit=mid; 
							diff=value-power;
						}
					}
				}
			}
		}
		return result;
	}

	private double getPower(double num, int rootNum) {
		double result=1;
		for(int i=0;i<rootNum;i++) {
			result=result*num;
		}
		return result;
	}

}
