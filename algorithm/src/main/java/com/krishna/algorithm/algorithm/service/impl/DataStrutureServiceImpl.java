package com.krishna.algorithm.algorithm.service.impl;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.krishna.algorithm.algorithm.Constant;
import com.krishna.algorithm.algorithm.entity.AvlTree;
import com.krishna.algorithm.algorithm.entity.Entity;
import com.krishna.algorithm.algorithm.entity.MyMap;
import com.krishna.algorithm.algorithm.entity.Tree;
import com.krishna.algorithm.algorithm.service.DataStructure;

/**
 * service to implement various searching and sorting
 * @author india
 *
 */
@Service
public class DataStrutureServiceImpl implements DataStructure{
	
	private Entity entity=new Entity();
	private Tree treeRoot=null;
	private AvlTree avlTreeRoot=null;
	
	private MyMap[] maps=null;
	private int mapSize=0;

	/**
	 * this api create a linked list and value to linked list,it add value to
	 * existing api
	 */
	@Override
	public Map<String, Object> addNodeToLinkedList(int value) {
		
		Entity temp =entity;
		while(temp.getNext()!=null) {
			temp=temp.getNext();
		}
		Entity newEntity = new Entity();
		newEntity.setValue(value);
		temp.setNext(newEntity);
		Map<String, Object> mapConaainingLinkedList = new HashMap<>();
		mapConaainingLinkedList.put(Constant.LIST, entity);
		return mapConaainingLinkedList;
	}

	/**
	 * get element of linked list
	 */
	@Override
	public Entity getLinkedList() {
		return entity;
	}
	/**
	 * this api add node to a binary tree
	 */
	@Override
	public Tree addTreeNode(int value) {
		Tree tempRoot = treeRoot;
		boolean isElementAlraidyPresent=true;
		if(treeRoot==null) {
			treeRoot=new Tree();
			treeRoot.setValue(value);
			treeRoot.setLeft(null);
			treeRoot.setRight(null);
			isElementAlraidyPresent=false;

		}
		while(tempRoot!=null) {
			if(tempRoot.getValue()==value)
			{
				break;
			}
			if(tempRoot.getValue()<value) {
				
				if(tempRoot.getRight()==null) {
					Tree newNode = new Tree();
					newNode.setLeft(null);
					newNode.setRight(null);
					newNode.setValue(value);
					tempRoot.setRight(newNode);
					isElementAlraidyPresent=false;
					break;
				}
				else {
					tempRoot=tempRoot.getRight();
				}
			}
			if(tempRoot.getValue()>value) {
				if(tempRoot.getLeft()==null) {
					Tree newNode = new Tree();
					newNode.setLeft(null);
					newNode.setRight(null);
					newNode.setValue(value);
					tempRoot.setLeft(newNode);
					isElementAlraidyPresent=false;
					break;
				}else {
					tempRoot=tempRoot.getLeft();
				}
			}
		}
		if(isElementAlraidyPresent) {
			System.out.println(value+"allraidy present in tree");
		}
		return treeRoot;
	}
	/**
	 * this api delete node to a binary tree
	 */
	@Override
	public Tree getAndDeleteTreeNode(int value) {
		if(treeRoot!=null&&treeRoot.getValue()==value) {
			if(treeRoot.getLeft()==null&&treeRoot.getRight()==null) {
				treeRoot =null;
			}
			else if(treeRoot.getLeft()==null) {
				treeRoot=treeRoot.getRight();
			}else if(treeRoot.getRight()==null){
				treeRoot=treeRoot.getLeft();
			}else {
				if(treeRoot.getRight().getLeft()==null) {
					Tree rightChild = treeRoot.getRight();
					Tree leftChild = treeRoot.getLeft();
					rightChild.setLeft(leftChild);
					treeRoot=rightChild;
				}else {
					deleteNodeHavingNonNulChild(treeRoot);
				}
			}
		}else {
			Tree tempRoot=treeRoot;
			deleteNonRoot(value, tempRoot);
		}
		
		return treeRoot;
	}

	private void deleteNonRoot(int value, Tree tempRoot) {
		Tree parentNodeOfValue=getParentNodeOfValue(tempRoot,value);
		if(parentNodeOfValue==null) {
			System.out.println("element does not exist");
		}else {
			if(parentNodeOfValue.getLeft()!=null&&parentNodeOfValue.getLeft().getValue()==value) {
				if(parentNodeOfValue.getLeft().getRight()==null&&parentNodeOfValue.getLeft().getLeft()==null){
					parentNodeOfValue.setLeft(null);
				}else if(parentNodeOfValue.getLeft().getRight()==null) {
					Tree parentGrantChild = parentNodeOfValue.getLeft().getLeft();
					parentNodeOfValue.setLeft(parentGrantChild);
				}else if(parentNodeOfValue.getLeft().getLeft()==null){
					Tree parentGrantChild = parentNodeOfValue.getRight().getRight();
					parentNodeOfValue.setLeft(parentGrantChild);
				}else {
					if(parentNodeOfValue.getLeft().getRight().getLeft()==null) {
						Tree parentGrantChild = parentNodeOfValue.getLeft().getRight();
						parentNodeOfValue.setLeft(parentGrantChild);
					}else {
						deleteNodeHavingNonNulChild(parentNodeOfValue.getLeft());
					}
				}
			}else if(parentNodeOfValue.getRight()!=null&&parentNodeOfValue.getRight().getValue()==value) {
				if(parentNodeOfValue.getRight().getRight()==null&&parentNodeOfValue.getRight().getLeft()==null){
					parentNodeOfValue.setRight(null);
				}else if(parentNodeOfValue.getRight().getRight()==null) {
					Tree parentGrantChild = parentNodeOfValue.getRight().getLeft();
					parentNodeOfValue.setRight(parentGrantChild);
				}else if(parentNodeOfValue.getRight().getLeft()==null){
					Tree parentGrantChild = parentNodeOfValue.getRight().getRight();
					parentNodeOfValue.setRight(parentGrantChild);
				}else {
					if(parentNodeOfValue.getRight().getRight().getLeft()==null) {
						Tree parentGrantChild = parentNodeOfValue.getRight().getRight();
						parentNodeOfValue.setRight(parentGrantChild);
					}else {
						deleteNodeHavingNonNulChild(parentNodeOfValue.getRight());
					}
				}
			}
		}
	}

	private void deleteNodeHavingNonNulChild(Tree nodeDoBeDelete) {
		Tree tempNode = nodeDoBeDelete.getRight();
		while(tempNode!=null) {
			if(tempNode.getLeft().getLeft()==null) {
				break;
			}
			tempNode=tempNode.getLeft();
		}
		swap(nodeDoBeDelete,tempNode.getLeft());
		Tree rightNodeOfDeletingNode = tempNode.getLeft().getRight();
		tempNode.setLeft(rightNodeOfDeletingNode);
	}

	private void swap(Tree nodeDoBeDelete, Tree node) {
		int temp =nodeDoBeDelete.getValue();
		nodeDoBeDelete.setValue(node.getValue());
		node.setValue(temp);
	}

	private Tree getParentNodeOfValue(Tree tempRoot, int value) {
		Tree parentNodeOfValue = null;
		while(tempRoot!=null) {
			if(tempRoot.getLeft()!=null&&tempRoot.getLeft().getValue()==value) {
				parentNodeOfValue=tempRoot;
				break;
			}if(tempRoot.getRight()!=null&&tempRoot.getRight().getValue()==value) {
				parentNodeOfValue=tempRoot;
				break;
			}
			if(tempRoot.getValue()<value) {
				tempRoot=tempRoot.getRight();
			}else {
				tempRoot=tempRoot.getLeft();
			}
		}
		return parentNodeOfValue;
	}
	/*this api delete node to a AVLTREE tree
	 */
	@Override
	public AvlTree addNodeToAvlTree(int value) {
		AvlTree tempAvlTreeRoot = avlTreeRoot;
		boolean isElementAlraidyPresent=true;
		if(avlTreeRoot==null) {
			avlTreeRoot=new AvlTree();
			avlTreeRoot.setValue(value);
			avlTreeRoot.setLeft(null);
			avlTreeRoot.setRight(null);
			isElementAlraidyPresent=false;

		}
		while(tempAvlTreeRoot!=null) {
			if(tempAvlTreeRoot.getValue()==value)
			{
				break;
			}
			if(tempAvlTreeRoot.getValue()<value) {
				
				if(tempAvlTreeRoot.getRight()==null) {
					AvlTree newNode = new AvlTree();
					newNode.setLeft(null);
					newNode.setRight(null);
					newNode.setValue(value);
					tempAvlTreeRoot.setRight(newNode);
					isElementAlraidyPresent=false;
					break;
				}
				else {
					tempAvlTreeRoot=tempAvlTreeRoot.getRight();
				}
			}
			if(tempAvlTreeRoot.getValue()>value) {
				if(tempAvlTreeRoot.getLeft()==null) {
					AvlTree newNode = new AvlTree();
					newNode.setLeft(null);
					newNode.setRight(null);
					newNode.setValue(value);
					tempAvlTreeRoot.setLeft(newNode);
					isElementAlraidyPresent=false;
					break;
				}else {
					tempAvlTreeRoot=tempAvlTreeRoot.getLeft();
				}
			}
		}
		if(isElementAlraidyPresent) { 
			System.out.println(value+"allraidy present in tree");
		}else {
			AvlTree tempAvlTree=avlTreeRoot;
			rotate(tempAvlTree);
		}
		return avlTreeRoot;

	}

	private void rotate(AvlTree tempAvlTree) {
		if(tempAvlTree!=null) {
			rotate(tempAvlTree.getLeft());
			rotate(tempAvlTree.getRight());
			int diffHeihgt = getHeight(tempAvlTree.getLeft()) - getHeight(tempAvlTree.getRight());
			
			if(tempAvlTree==avlTreeRoot) {
				rotateRoot(tempAvlTree, diffHeihgt);
			}else {
				AvlTree parent=getParent(tempAvlTree);
				if(diffHeihgt>1) {
					AvlTree leftChild = tempAvlTree.getLeft();//b
					AvlTree rightChildOFLeftChild = tempAvlTree.getLeft().getRight();//e
					leftChild.setRight(tempAvlTree);
					tempAvlTree.setLeft(rightChildOFLeftChild);
					if(parent.getValue()<tempAvlTree.getValue()) {
						parent.setRight(leftChild);
					}else {
						parent.setLeft(leftChild);
					}
				}
				else if(diffHeihgt<-1) {
					AvlTree rightChild = tempAvlTree.getRight();//c
					AvlTree leftChildOfrightChild = tempAvlTree.getRight().getLeft();//f
					rightChild.setLeft(tempAvlTree);
					tempAvlTree.setRight(leftChildOfrightChild);
					if(parent.getValue()<tempAvlTree.getValue()) {
						parent.setRight(rightChild);
					}else {
						parent.setLeft(rightChild);
					}
				}
			}
		
		}
	}

	private AvlTree getParent(AvlTree child) {
		AvlTree tempAvlTree = avlTreeRoot;
		AvlTree parent =null;
		while(tempAvlTree!=null) {
			if(tempAvlTree.getLeft()==child||tempAvlTree.getRight()==child) {
				parent=tempAvlTree;
				break;
			}
			if(tempAvlTree.getValue()<child.getValue()) {
				tempAvlTree=tempAvlTree.getRight();
			}else {
				tempAvlTree=tempAvlTree.getLeft();
			}
		}
		return parent;
	}

	private void rotateRoot(AvlTree tempAvlTree, int diffHeihgt) {
		if(diffHeihgt>1) {
			AvlTree leftChild = tempAvlTree.getLeft();//b
			AvlTree rightChildOFLeftChild = tempAvlTree.getLeft().getRight();//e
			leftChild.setRight(tempAvlTree);
			tempAvlTree.setLeft(rightChildOFLeftChild);
			avlTreeRoot=leftChild;
		}
		else if(diffHeihgt<-1) {
			AvlTree rightChild = tempAvlTree.getRight();//c
			AvlTree leftChildOfrightChild = tempAvlTree.getRight().getLeft();//f
			rightChild.setLeft(tempAvlTree);
			tempAvlTree.setRight(leftChildOfrightChild);
			avlTreeRoot=rightChild;
		}
	}

	private int getHeight(AvlTree tempAvlTree) {
		if(tempAvlTree==null) {
			return -1;
		}
		return max(getHeight(tempAvlTree.getLeft()),getHeight(tempAvlTree.getRight()))+1;
	}

	private int max(int height, int height2) {
		int max=height;
		
		if(height>height2) {
			max=height;
		}else {
			max=height2;
			}
		return max;
	}

	@Override
	public MyMap[] addNodeToMap(String key, String value) {
		MyMap newNode=new MyMap();
		newNode.setKey(key);
		newNode.setValue(value);
		newNode.setNext(null);
		int hashCode = getHashCode(key);
		if(maps==null) {
			maps=new MyMap[10];
			
			int modVaue=hashCode%10;
			putMapValue(newNode, modVaue);
		}else {
			int lenghth=maps.length;
			int modVaue=hashCode%lenghth;
			putMapValue(newNode, modVaue);
		}
		if(maps.length<mapSize+2) {
			reset();
		}
		return maps;
	}

	private void reset() {
		int newLegth =maps.length*2;
		MyMap[] temp=maps;
		maps=new MyMap[newLegth];
		for(MyMap map:temp) {
			if(map!=null) {
				while(map!=null) {
					int hashCode = getHashCode(map.getKey());
					int modVaue=hashCode%newLegth;
					putMapValue(map, modVaue);
					map=map.getNext();
				}
			}
		}
	}

	private void putMapValue(MyMap newNode, int modVaue) {
		if(modVaue<0) {
			modVaue*=-1;
		}
		if(maps[modVaue]==null) {
			maps[modVaue]=newNode;
			mapSize++;
		}else {
			setMapVlaue(maps[modVaue],newNode);
			mapSize++;
		}
	}

	private int getHashCode(String key) {
		int hashCode = key.hashCode();
		if(hashCode<0) {
			hashCode*=-1;
		}else {
			hashCode*=2;
		}
		return hashCode;
	}

	private void setMapVlaue(MyMap myMap, MyMap newNode) {
		while(myMap!=null) {
			if(myMap.getKey().equals(newNode.getKey())) {
				myMap.setValue(newNode.getValue());
				break;
			}else if(myMap.getNext()==null) {
				myMap.setNext(newNode);
			}
			myMap=myMap.getNext();

		}
		
	}
}
