package com.krishna.algorithm.algorithm.service;

import java.util.Map;

import org.springframework.stereotype.Component;

import com.krishna.algorithm.algorithm.entity.AvlTree;
import com.krishna.algorithm.algorithm.entity.Entity;
import com.krishna.algorithm.algorithm.entity.MyMap;
import com.krishna.algorithm.algorithm.entity.Tree;


@Component
public interface DataStructure {

	Map<String, Object> addNodeToLinkedList(int value);

	Entity getLinkedList();

	Tree addTreeNode(int value);

	Tree getAndDeleteTreeNode(int value);

	AvlTree addNodeToAvlTree(int value);

	MyMap[] addNodeToMap(String key, String value);
}
