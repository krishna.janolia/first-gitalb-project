package com.krishna.algorithm.algorithm.service;

import org.springframework.stereotype.Component;


@Component
public interface NumberSeriesService {

	int[] getCatlinNumber(int value);

	double getNthRoot(int value, int rootNum);

}
