package com.krishna.algorithm.algorithm.service.impl;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.krishna.algorithm.algorithm.entity.GraphNode;
import com.krishna.algorithm.algorithm.entity.WightedGraphNode;
import com.krishna.algorithm.algorithm.service.GraphService;

/**
 * service to implement various Graph searching and sorting
 * @author Krishna janoliya
 *
 */
@Service
public class GraphServiceImpl implements GraphService{
	
	private GraphNode graphNode = null;
	WightedGraphNode wightedGraphNode = null;
	private Map<Integer, Boolean> visit =new HashMap<>();
	private LinkedList<GraphNode> frontiers= new LinkedList();
	private LinkedList<WightedGraphNode> frontiersWightedGraph= new LinkedList();

	@Override
	public Boolean addNodeToGraph(int source, int destinetion) {
		if (graphNode == null) {
			graphNode = new GraphNode();
			graphNode.setValue(source);
			GraphNode newGraphNode = new GraphNode();
			newGraphNode.setValue(destinetion);
			graphNode.getGraphs().add(newGraphNode);
		} else {
			GraphNode sourceNode = getSourceNode(source);
			if (sourceNode == null) {
				return false;
			}
			GraphNode detinationNode = getSourceNode(destinetion);
			if(detinationNode!=null) {
				for (GraphNode graphNode : sourceNode.getGraphs()) {
					if(graphNode.getValue()==destinetion) {
						return false;
					}
				}
				sourceNode.getGraphs().add(detinationNode);
			}else {
				GraphNode newGraphNode = new GraphNode();
				newGraphNode.setValue(destinetion);
				sourceNode.getGraphs().add(newGraphNode);
			}
		}
		return true;
	}

	private GraphNode getSourceNode(int source) {
		GraphNode sourceNode = null;
		visit = new HashMap<>();
		frontiers.addFirst(graphNode);
		boolean found = false;
		while (!frontiers.isEmpty()) {
			GraphNode temp = frontiers.removeLast();
			if (temp.getValue() == source) {
				sourceNode = temp;
				break;
			}
			visit.put(temp.getValue(), true);
			List<GraphNode> graphNodes = temp.getGraphs();
			for (GraphNode graphNode : graphNodes) {
				if (graphNode.getValue() == source) {
					found = true;
					sourceNode = graphNode;
					break;
				}
				if (visit.get(graphNode.getValue()) == null) {
					frontiers.addFirst(graphNode);
				}

			}
			if (found) {
				break;
			}
		}
		return sourceNode;
	}

	/**
	 * 
	 */
	@Override
	public Boolean BFS(int value) {
		boolean found =false;
		GraphNode graphNode = getSourceNode(value);
		if(graphNode!=null) {
			found=true;
		}
		return found;
	}

	@Override
	public Boolean DFS(int value) {
		boolean found =false;
		visit =new HashMap<>();
		GraphNode nodeToBeSearch=findNodeByDfs(visit,graphNode,value);
		if(nodeToBeSearch!=null) {
			found=true;
		}
		return found;
	}

	private GraphNode findNodeByDfs(Map<Integer, Boolean> visitDFS, GraphNode graphNodeDFS, int value) {
		GraphNode nodeToBeSearch=null;
		if(graphNodeDFS.getValue()==value) {
			nodeToBeSearch = graphNodeDFS;
		}else {
			visitDFS.put(graphNodeDFS.getValue(), true);
			for (GraphNode graphNode : graphNodeDFS.getGraphs()) {
				if(visitDFS.get(graphNode.getValue())==null) {
					findNodeByDfs(visitDFS, graphNode, value);
				}
			}
		}
		return nodeToBeSearch;
	}

	@Override
	public Boolean addNodeToWeightedGraph(int source, int destinetion , int weight) {
		if (wightedGraphNode == null) {
			wightedGraphNode = new WightedGraphNode();
			wightedGraphNode.setLocationId(source);
			WightedGraphNode newGraphNode = new WightedGraphNode();
			newGraphNode.setLocationId(destinetion);;
			wightedGraphNode.getConnectedNodes().add(newGraphNode);
			wightedGraphNode.getWeightOfNodes().add(weight);
		} else {
			WightedGraphNode sourceNode = getSourceNodeOfWeightedGraph(source);
			if (sourceNode == null) {
				return false;
			}
			WightedGraphNode detinationNode = getSourceNodeOfWeightedGraph(destinetion);
			if(detinationNode!=null) {
				for (WightedGraphNode graphNode : sourceNode.getConnectedNodes()) {
					if(graphNode.getLocationId()==destinetion) {
						return false;
					}
				}
				sourceNode.getConnectedNodes().add(detinationNode);
				sourceNode.getWeightOfNodes().add(weight);
			}else {
				WightedGraphNode newGraphNode = new WightedGraphNode();
				newGraphNode.setLocationId(destinetion);
				sourceNode.getConnectedNodes().add(newGraphNode);
				sourceNode.getWeightOfNodes().add(weight);
			}
		}
		return true;

	}
	
	private WightedGraphNode getSourceNodeOfWeightedGraph(int source) {
		WightedGraphNode sourceNode = null;
		visit = new HashMap<>();
		frontiersWightedGraph = new LinkedList<>();
		frontiersWightedGraph.addFirst(wightedGraphNode);
		boolean found = false;
		while (!frontiersWightedGraph.isEmpty()) {
			WightedGraphNode temp = frontiersWightedGraph.removeLast();
			if (temp.getLocationId() == source) {
				sourceNode = temp;
				break;
			}
			visit.put(temp.getLocationId(), true);
			List<WightedGraphNode> graphNodes = temp.getConnectedNodes();
			for (WightedGraphNode graphNode : graphNodes) {
				if (graphNode.getLocationId() == source) {
					found = true;
					sourceNode = graphNode;
					break;
				}
				if (visit.get(graphNode.getLocationId()) == null) {
					frontiersWightedGraph.addFirst(graphNode);
				}

			}
			if (found) {
				break;
			}
		}
		return sourceNode;
	}

	@Override
	public Integer dijkstra(int source, int destinetion) {
		int distence = -1;
		WightedGraphNode sourceNode = getSourceNodeOfWeightedGraph(source);
		WightedGraphNode destinetionNode = getSourceNodeOfWeightedGraph(destinetion);
		if(sourceNode!=null && destinetionNode!=null) {
			frontiersWightedGraph = new LinkedList<>();
			visit = new HashMap<>();
			if(sourceNode == destinetionNode) {
				distence = 0;
			}else {
				sourceNode.setFromLocationId(sourceNode.getLocationId());
				sourceNode.setWeight(0);
				visit.put(sourceNode.getLocationId(), true);
				frontiersWightedGraph.addFirst(sourceNode);
				while(!frontiersWightedGraph.isEmpty()) {
					WightedGraphNode temp = frontiersWightedGraph.removeLast();
					for(int index =0; index < temp.getConnectedNodes().size();index++) {
						int weight = temp.getWeightOfNodes().get(index);
						WightedGraphNode wightedGraphNode = temp.getConnectedNodes().get(index);
						if(wightedGraphNode.getFromLocationId() != sourceNode.getLocationId()) {
							wightedGraphNode.setFromLocationId(sourceNode.getLocationId());
							wightedGraphNode.setWeight(temp.getWeight()+weight);
						}else {
							int totalWight = temp.getWeight()+weight;
							if(wightedGraphNode.getWeight()>totalWight) {
								wightedGraphNode.setWeight(totalWight);
							}
						}
						if(visit.get(wightedGraphNode.getLocationId())==null) {
							frontiersWightedGraph.addFirst(wightedGraphNode);
						}
					}
					
				}
				distence = destinetionNode.getWeight();
			}
		}
		return distence;
	}

	
}
