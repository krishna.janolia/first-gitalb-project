package com.example.basic.entity;

public class ProductNotFound extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	@Override
	public String toString() {
		return "ProductNotFound [name=" + name + "]";
	}
	private String name;
	public ProductNotFound(String name) {
		super();
		this.name = name;
	}
	
	public ProductNotFound() {
	}
}
