package com.example.basic;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
public class BasicApplication {
	@Value("${name}")
	public static String name;

	public static void main(String[] args) {
		SpringApplication.run(BasicApplication.class, args);
		System.out.print(name);
	}
	
	@Bean
	public RestTemplate getrestTemplate() {
		return new RestTemplate();
	}

}
