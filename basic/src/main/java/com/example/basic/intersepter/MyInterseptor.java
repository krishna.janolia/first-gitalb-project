package com.example.basic.intersepter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class MyInterseptor implements Filter{
	
	Logger logger = LoggerFactory.getLogger(Filter.class);
	
	@Override
   public void destroy() {}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		logger.info("iside doFilter");
		chain.doFilter(request, response);
		
	}
	
	@Override
	   public void init(FilterConfig filterconfig) throws ServletException {}


}
