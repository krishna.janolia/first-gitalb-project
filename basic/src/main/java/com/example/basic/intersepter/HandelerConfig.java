package com.example.basic.intersepter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

public class HandelerConfig extends WebMvcConfigurerAdapter{
	
	@Autowired
	private MyIntrseptor myIntrseptor;

	@Override
	public void addInterceptors(InterceptorRegistry registry) {
		registry.addInterceptor(myIntrseptor);
		super.addInterceptors(registry);
	}

}
