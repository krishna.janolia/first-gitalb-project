package com.example.basic.controller;
import com.example.basic.Constents;

import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.RestTemplate;

import com.example.basic.service.MyService;

@Controller
@RequestMapping("/my")
public class MyController {
	
	@Autowired
	private MyService myService;
	
	@Autowired
	private RestTemplate restTemplate;
	
	@GetMapping("/print/hello/from/property/file")
	public ResponseEntity<String> hello() {
		return new ResponseEntity<String>(myService.hello(), HttpStatus.OK);
		
	}
	
	@GetMapping(Constents.PRODUCT + Constents.NOT + Constents.FOUND)
	public ResponseEntity<String> productNotFound(@RequestParam("name") String name) {
		return new ResponseEntity<String>(myService.productNotFound(name), HttpStatus.OK);
		
	}
	
	@GetMapping(Constents.PRINT + Constents.PROPERTY + Constents.FILE + Constents.BY + Constents.REST_TEMPLET )
	public ResponseEntity<String> printPropertyFileByRestTemplet(@RequestParam("name") String name) {
		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
		HttpEntity<String> entity  = new HttpEntity<>(httpHeaders);
		return new ResponseEntity<String>(restTemplate.exchange("http://localhost:9090/my/print/hello/from/property/file", HttpMethod.GET, entity, String.class).getBody(), HttpStatus.OK);
		
	}
	

}
