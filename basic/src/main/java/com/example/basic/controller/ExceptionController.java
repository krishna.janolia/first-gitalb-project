package com.example.basic.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.example.basic.entity.ProductNotFound;

@ControllerAdvice
public class ExceptionController {
	
	@ExceptionHandler(value = ProductNotFound.class)
	public ResponseEntity<String> productNotFound(ProductNotFound productNotFound){
		return new ResponseEntity<>(productNotFound.toString(), HttpStatus.NOT_FOUND);
	}

}
