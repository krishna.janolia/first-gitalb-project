package com.example.basic.service;

import org.springframework.stereotype.Service;

@Service
public interface MyService {

	String hello();

	String productNotFound(String name);

}
