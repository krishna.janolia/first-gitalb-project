package com.example.basic.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.example.basic.entity.ProductNotFound;
import com.example.basic.service.MyService;

@Service
public class MyServiceImpl implements MyService{
	
	@Value("${name}")
	private String hello;
	
 private static final Logger logger = LoggerFactory.getLogger(MyServiceImpl.class);


	@Override
	public String hello() {
		logger.info("info");
		return hello;
	}


	@Override
	public String productNotFound(String neme)
			{
		if(!neme.equals(hello)) {
			throw new ProductNotFound(neme);
		}
		return hello;
	}

}
